using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using FindFriend.Data;
using FindFriend.Models;

namespace FindFriend.Pages.MyDataAdmin
{
    public class EditModel : PageModel
    {
        private readonly FindFriend.Data.FindFriendContext _context;

        public EditModel(FindFriend.Data.FindFriendContext context)
        {
            _context = context;
        }

        [BindProperty]
        public MyData MyData { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            MyData = await _context.dataList
                .Include(m => m.DataCat).FirstOrDefaultAsync(m => m.MyDataID == id);

            if (MyData == null)
            {
                return NotFound();
            }
           ViewData["LocationID"] = new SelectList(_context.Location, "LocationID", "LocationID");
            return Page();
        }

        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            _context.Attach(MyData).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!MyDataExists(MyData.MyDataID))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return RedirectToPage("./Index");
        }

        private bool MyDataExists(int id)
        {
            return _context.dataList.Any(e => e.MyDataID == id);
        }
    }
}
