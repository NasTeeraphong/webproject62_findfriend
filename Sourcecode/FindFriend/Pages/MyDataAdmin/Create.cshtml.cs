using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using FindFriend.Data;
using FindFriend.Models;

namespace FindFriend.Pages.MyDataAdmin
{
    public class CreateModel : PageModel
    {
        private readonly FindFriend.Data.FindFriendContext _context;

        public CreateModel(FindFriend.Data.FindFriendContext context)
        {
            _context = context;
        }

        public IActionResult OnGet()
        {
        ViewData["LocationID"] = new SelectList(_context.Location, "LocationID", "ShortName");
            return Page();
        }

        [BindProperty]
        public MyData MyData { get; set; }

        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            _context.dataList.Add(MyData);
            await _context.SaveChangesAsync();

            return RedirectToPage("./Index");
        }
    }
}