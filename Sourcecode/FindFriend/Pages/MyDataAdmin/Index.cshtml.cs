using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

using Microsoft.EntityFrameworkCore;
using FindFriend.Data;
using FindFriend.Models;

namespace FindFriend.Pages
{
    public class IndexModel : PageModel
    {
        private readonly FindFriend.Data.FindFriendContext _context;

        public IndexModel(FindFriend.Data.FindFriendContext context)
        {
            _context = context;
        }

        public IList<MyData> MyData { get;set; }

        public async Task OnGetAsync()
        {
            MyData = await _context.dataList
                .Include(m => m.DataCat).ToListAsync();
        }
    }
}
