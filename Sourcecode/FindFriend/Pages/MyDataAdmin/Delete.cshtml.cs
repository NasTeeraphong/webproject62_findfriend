using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using FindFriend.Data;
using FindFriend.Models;

namespace FindFriend.Pages.MyDataAdmin
{
    public class DeleteModel : PageModel
    {
        private readonly FindFriend.Data.FindFriendContext _context;

        public DeleteModel(FindFriend.Data.FindFriendContext context)
        {
            _context = context;
        }

        [BindProperty]
        public MyData MyData { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            MyData = await _context.dataList
                .Include(m => m.DataCat).FirstOrDefaultAsync(m => m.MyDataID == id);

            if (MyData == null)
            {
                return NotFound();
            }
            return Page();
        }

        public async Task<IActionResult> OnPostAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            MyData = await _context.dataList.FindAsync(id);

            if (MyData != null)
            {
                _context.dataList.Remove(MyData);
                await _context.SaveChangesAsync();
            }

            return RedirectToPage("./Index");
        }
    }
}
