using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using FindFriend.Data;
using FindFriend.Models;

namespace FindFriend.Pages.MyDataAdmin
{
    public class DetailsModel : PageModel
    {
        private readonly FindFriend.Data.FindFriendContext _context;

        public DetailsModel(FindFriend.Data.FindFriendContext context)
        {
            _context = context;
        }

        public MyData MyData { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            MyData = await _context.dataList
                .Include(m => m.DataCat).FirstOrDefaultAsync(m => m.MyDataID == id);

            if (MyData == null)
            {
                return NotFound();
            }
            return Page();
        }
    }
}
