using FindFriend.Models; 
using Microsoft.EntityFrameworkCore; 
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;

namespace FindFriend.Data
{
    public class FindFriendContext : IdentityDbContext<MyDataUser> 
    {
        public DbSet<MyData> dataList {get; set;}
        public DbSet<Location> Location {get; set;}

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
             optionsBuilder.UseSqlite(@"Data source= FindReport.db");
        }
    }
}