using System;
using System.ComponentModel.DataAnnotations; 
using Microsoft.AspNetCore.Identity; 

namespace FindFriend.Models
{
    public class Location{
        public int LocationID {get; set;}
        public string ShortName {get; set;}
        public string FullName {get; set;}
    }

    public class MyData{
        public int MyDataID{get; set;}
        public int LocationID{get; set;}
        public Location DataCat{get; set;}

        public string Name {get; set;}
        public string NickName {get; set;}
        public float Year {get; set;}
        public string DataDetail {get; set;}
        public string SubJect {get; set;}
        public string ReportDate {get; set;}
        public string Facebook {get; set;}
        public string TellNumber {get; set;} 

        public string MyDataUserId {get; set;}    
        public MyDataUser postUser {get; set;}
    }

    public class MyDataUser :  IdentityUser{         
        public string  FirstName { get; set;}         
        public string  LastName { get; set;}     
    }
}